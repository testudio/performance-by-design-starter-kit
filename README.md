# Performance-by-design Starter Kit

This project covers best practices for **Drupal** CMS for:
- performance optimisation
- profiling
- monitoring
- trouble shooting
- production checklist

This project a sister project of [Security-by-design starter kit](https://gitlab.com/testudio/security-by-design-starter-kit) for Drupal

**Hold on! More to come!**

## Performance-by-design project

## TL;DR;

## Support
- Report bugs and request features in the Gitlab Issues.
- Use merge requests (MRs) to contribute to this project.

## License
CC0 1.0 Universal - CC0 public domain

### Resources Attribution
<a href="https://www.freepik.com/icon/electricity_2270697#fromView=search&term=Flash+blue&track=ais&page=1&position=62&uuid=ed9b7fa6-06e6-43b2-a26c-048d3a12eb3a">Icon by xnimrodx</a>
